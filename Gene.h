#pragma once
#include <iostream>

class Gene
{
private:
	// fields
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	bool is_on_complementary_dna_strand() const;
	unsigned int get_start() const;
	unsigned int get_end() const;
	void Set_on_complementary_dna_strand(const bool on_complementary_dna_strand);
	void Set_start(const unsigned int start);
	void Set_end(const unsigned int end);
};

