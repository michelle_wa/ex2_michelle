#include <iostream>
#include <string>
#include "Nucleus.h"


void Nucleus::init(const std::string dna_sequence)
{
	int i = 0;
	this->_DNA_strand = dna_sequence;
	std::string complementary;
	for (i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] == 'G')
		{
			complementary += 'C';
		}	
		else if (dna_sequence[i] == 'C')
		{
			complementary += 'G';
		}
		else if (dna_sequence[i] == 'A')
		{
			complementary += 'T';
		}	
		else if (dna_sequence[i] == 'T')
		{
			complementary += 'A';
		}
		else
		{
			std::cerr << "error1" << std::endl;
			_exit(1);
		}
	}
	this->_complementary_DNA_strand = complementary;
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	int i = 0;
	char ch;
	std::string str;
	if (gene.is_on_complementary_dna_strand() == false) //DNA 1
	{
		for (i = gene.get_start(); i <= gene.get_end(); i++)
		{
			if (this->_DNA_strand[i] == 'T')
			{
				ch = 'U';
			}
			else
			{
				ch = this->_DNA_strand[i];
			}
			str = str + ch;
		}
	}
	else  //DNA 2 (complementary)
	{
		for (i = gene.get_start(); i <= gene.get_end(); i++)
		{
			if (this->_complementary_DNA_strand[i] == 'T')
			{
				ch = 'U';
			}
			else
			{
				ch = this->_complementary_DNA_strand[i];
			}
			str = str + ch;
		}
	}
	return str;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string str = this->_DNA_strand;
	std::string str2 = "";
	int i = 0;
	for (i = str.length() - 1; i >= 0; i--)
	{
		str2 = str2 + str[i];
	}
	return str2;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int counter = 0;
	std::string::size_type p = 0;
	while ((p = this->_DNA_strand.find(codon, p)) != std::string::npos) {
		counter++;
		p += codon.length();
	}
	return counter;
}