#include <iostream>
#include "AminoAcid.h"
#include "Mitochondrion.h"
#include "Protein.h"

void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	AminoAcidNode* temp = new AminoAcidNode;
	if (protein.get_first() != NULL)
	{
		temp = protein.get_first();
		if (temp->get_data() == ALANINE)
		{
			temp = temp->get_next();
			if (temp->get_data() == LEUCINE)
			{
				temp = temp->get_next();
				if (temp->get_data() == GLYCINE)
				{
					temp = temp->get_next();
					if (temp->get_data() == HISTIDINE)
					{
						temp = temp->get_next();
						if (temp->get_data() == LEUCINE)
						{
							temp = temp->get_next();
							if (temp->get_data() == PHENYLALANINE)
							{
								temp = temp->get_next();
								if (temp->get_data() == AMINO_CHAIN_END)
								{
									this->_has_glocuse_receptor = true;
								}
							}
						}
					}
				}
			}
		}
	}
}
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}
bool Mitochondrion::produceATP() const
{
	if (this->_glocuse_level >= 50 && this->_has_glocuse_receptor)
	{
		return true;
	}
	else
	{
		return false;
	}
}