#include <iostream>
#include "Ribosome.h"

Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{
	Protein* protein1 = new Protein;
	std::string RNA_str = RNA_transcript;
	std::string codon = "";

	protein1->init();
	while (RNA_str.size() > 2)
	{
		codon = RNA_str.substr(0, 3);
		if (get_amino_acid(codon) != UNKNOWN)
		{
			RNA_str = RNA_str.substr(3, RNA_str.size());
			protein1->add(get_amino_acid(codon));
		}
		else
		{
			protein1->clear();
			return nullptr;
		}
	}
	return protein1;
}