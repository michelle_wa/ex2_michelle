#pragma once
#include <iostream>
#include <string>
#include "Gene.h"

class Nucleus
{
private:
	// fields
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;
public:
	void init(const std::string dna_sequence);
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
};
