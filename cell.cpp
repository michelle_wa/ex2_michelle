#include <iostream>
#include "cell.h"
#include "Mitochondrion.h"
#include "Nucleus.h"
#include "Protein.h"
#include "Ribosome.h"

void cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_mitochondrion.init();
}
bool cell::get_ATP() //energy
{
	std::string transcript = "";
	Protein* Protein1 = new Protein;
	transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	Protein1 = this->_ribosome.create_protein(transcript);
	if (!Protein1) // Didnt succeed
	{
		std::cerr << "ERROR" << std::endl;
		_exit(1);
	}
	this->_mitochondrion.insert_glucose_receptor(*Protein1);
	this->_mitochondrion.set_glucose(50);
	if (!this->_mitochondrion.produceATP()) //Energy production failed
	{
		return false;
	}
	else
	{
		this->_mitochondrion._glocuse_level = 100;
		return true;
	}
}