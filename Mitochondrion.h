#pragma once
#include <iostream>
#include "Protein.h"

class Mitochondrion
{
private:
	// fields
	bool _has_glocuse_receptor;
public:
	unsigned int _glocuse_level;
	void init();
	void insert_glucose_receptor(const Protein& protein);
	void set_glucose(const unsigned int glocuse_units);
	bool produceATP() const;
};
